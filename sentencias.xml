<chapter>
<title>Sentencias SQL</title>

<section>
<title>Consultando datos</title>
<para>La sentencia SELECT se usa para consultar la base de datos y obtener información que cumpla con los criterios especificados. Este es el formato de un SELECT sencillo:</para>
<screen>
select "columna1"
  [,"columna2",etc] 
  from "tabla"
  [where "condiciones"];
</screen>
<para>Los nombres de columnas que siguen a la palabra clave SELECT determinan cuales columnas deben regresar en los resultados. Se pueden especificar tantas columnas como se desee o se puede usar el comodín (*) asterisco para regresar todas las columnas.</para>
<para>El nombre de la tabla que aparece después de la palabra clave FROM específica la tabla de donde se obtendrán los resultados esperados.</para>
<para>La clausula WHERE (opcional) especifica cuales valores de datos o registros serán regresados, basado en el criterio descrito después de la palabra clave WHERE.</para>
<table>
<title>Operadores condicionales</title>
<tgroup cols="2">
<!-- thead>
<row><entry>Operador</entry><entry></row>
</thead -->
<tbody>
<row><entry>=</entry><entry>Igual</entry></row>
<row><entry>&gt;</entry><entry>Mayor que</entry></row>
<row><entry>&lt;</entry><entry>Menor que</entry></row>
<row><entry>&gt;=</entry><entry>Mayor o igual que</entry></row>
<row><entry>&lt;=</entry><entry>Menor o igual que</entry></row>
<row><entry>&lt;&gt;</entry><entry>No igual a</entry></row>
<row><entry>LIKE</entry><entry>*Ver la nota abajo.</entry></row>
</tbody>
</tgroup>
</table>
<para>El operador LIKE se usa en combinación con WHERE. LIKE es un operador que permite seleccionar registros que son "como" lo que se especifica, sin ser exactamente igual. El signo de porcentaje "%" es el comodín que se puede usar para hacer coincidir cualquier carácter posible que pudiera aparecer antes o después de los caracteres especificados. Por ejemplo:
<screen>
select ArtistId, Name from Artist where Name Like 'Me%';

50|Metallica
105|Men At Work
271|Mela Tenenbaum, Pro Musica Prague &amp; Richard Kapp
</screen>
</para>
<para>Esta sentencia SQL regresará los registros donde el nombre empiece con 'Me' sin importar que caracteres aparezcan después. Los caracteres alfa-numéricos deben ir entre comillas sencillas.</para>
<para>O se puede especificar,
<screen>
select Name from Artist where Name like '%g';

Aaron Goldberg
Temple of the Dog
</screen>
</para>
<para>Esta sentencia regresará los registros donde el nombre del artista termine con una 'g'.
<screen>
select * from Artist where Name = 'Metallica';

50|Metallica
</screen>
</para>
<para>Esta consulta solo regresa los registros donde el artista sea 'Metallica' exactamente.</para>

<section>
<title>Ejercicios de SELECT</title>
<para>
Escribe las siguientes sentencias de ejemplo en el interprete de SQLite. Antes de ejecutar las consultas anota los resultados que esperas y compara los resultados.</para>
<screen>
select FirstName, LastName, City from Employee;

select FirstName, LastName, City from Employee where City='Lethbridge';

select FirstName, LastName, City, State from Employee where FirstName like 'J%';

select * from Employee;
</screen>
<para>Ejercicios opcionales:
<itemizedlist>
<listitem>Muestra el primer nombre y fecha de nacimiento de todos los empleados.</listitem>
<listitem>Muestra el primer nombre y fecha de nacimiento de todos los empleados cuyo puesto no sea 'IT Staff'.</listitem>
<listitem>Muestra todos los empleados cuyo Apellido termine con la letra 'k'</listitem>
    </itemizedlist>
</para></section>
</section>

<section>
<title>Creando tablas</title>
<para>La sentencia CREATE TABLE se usa para crear nuevas tablas. El formato para crear una tabla sencilla es:</para>
<screen>
create table "nombre_tabla"
("columna1" "tipo de datos",
 "columna2" "tipo de datos",
 "columna3" "tipo de datos");
</screen>
<para>Formato de CREATE TABLE si se fueran a usar restricciones opcionales:
<screen>
create table "nombre_tabla"
("columna1" "tipo de datos" 
         [restricciones],
 "columna2" "tipo de datos" 
         [restricciones],
 "columna3" "tipo de datos" 
        [restricciones]);
</screen>
<note><para>Se pueden tener tantas columnas como sea necesario y las restricciones son opcionales.</para></note>
</para>
<para>Ejemplo:
<screen>
create table aficiones (id int not null primary key,
   descripcion varchar(50));
</screen>
El ejemplo muestra como crear una tabla nueva. Es importante usar paréntesis para encerrar la definición de las columnas en la tabla, la definición de cada columna va separada con una <quote>,</quote> (coma) y todas las sentencias SQL deben terminar con <quote>;</quote> (punto y coma).</para>
<para>Los nombres de las columnas deben iniciar con una letra y pueden incluir letras, números o guión bajo sin exceder un total de 30 caracteres de longitud. No se deben usar palabras reservadas de SQL como nombres de tablas o columnas (como <quote>select</quote>, <quote>create</quote>, <quote>insert</quote>, etc).</para>
<para>Los tipos de datos especifican que tipo de datos se van a almacenar en cada columna. Si una columna llamada <quote>Apellido</quote> se usa para almacenar apellidos, entonces esa columna en particular debería ser de tipo <quote>varchar</quote> (caracteres de longitud variable).
<table>
<title>Tipos de Datos Comunes</title>
<tgroup cols="2">
<tbody>
<row><entry>char(tamaño)</entry><entry>Cadena de caracteres de longitud fija. Longitud máxima se especifica entre paréntesis. Longitud máxima 255.</entry></row>
<row><entry>varchar(tamaño)</entry><entry>Cadena de caracteres de longitud variable. Longitud máxima se especifica entre paréntesis.</entry></row>
<row><entry>integer</entry><entry>Número entero</entry></row>
<row><entry>date</entry><entry>Fecha</entry></row>
<row><entry>real</entry><entry>Número de punto flotante.</entry></row>
</tbody></tgroup>
</table>
</para>
<para>Cuando se crean las tablas es común que una o más columnas tengan  restricciones asociadas a ellas. Una restricción es básicamente una regla asociada con una columna que la información almacenada en esa columna debe seguir. Por ejemplo una restricción <quote>unique</quote> especifica que no más de un registro puede tener el mismo valor en una columna en particular. La otras dos restricciones más populares son <quote>not null</quote> que especifica que una columna no puede quedar vácia (sin información) y <quote>primary key</quote>. Una restricción de clave primaria, que identifica únicamente a cada registro (o renglón) en una tabla. Todas estas y más serán cubiertas en este curso.</para>
</section>

<section>
<title>Insertar en una tabla</title>
<para>La sentencia <quote>insert into</quote> se usa para agregar un registro de datos en una tabla.
La palabra reservada <quote>values</quote> se usa para especificar los valores que se van a insertar en cada una de las columnas de la tabla. Se pueden especificar las columnas entre paréntesis y separadas por <quote>,</quote> (comas). Las cadenas de texto deben ir encerradas entre comillas simples.
<screen>
insert into "nombre_tabla"
 (primera_columna,...última_columna)
  values (primer_valor,...último_valor);
</screen>
En el ejemplo siguiente el nombre de columna id coicide con el valor 1 y la columna descripción coincide con el valor 'Leer'.
<screen>
insert into aficiones (id, descripcion) values (1,'Leer');
</screen>
<note>Todas las cadenas de texto deben ir encerradas entre comillas simples: 'string'</note>
</para>
<section>
<title>Ejercicios de INSERT</title>
<para>
<screen>
insert into Album (Title, ArtistId) values ('Best of...', 50);

select * from Album order by AlbumId desc limit 1;
</screen>
Esta sentencia agrega un álbum nuevo de Metallica, como la columna ArtistId es <quote>AUTO INCREMENT</quote>, podemos omitir especificar este valor en la sentencia.</para>
</section>
</section>

<section>
<title>Actualizando registros</title>
<para>La sentencia <quote>update</quote> se usa para actualizar registros que coinciden con un criterio especificado. Esto se logra construyendo cuidadosamente una clausula <quote>where</quote>.
<screen>
update "nombre_tabla"
set "columna1" = 
    "nuevo_valor1"
 [,"columna2" = 
   "nuevo_valor2"...]
where "columna" OPERADOR "valor" 
 [y|o "columna" OPERADOR "valor"];
</screen>
</para>
<para>Ejemplos:
<screen>
update Genre
  set Name = 'Electronic/Dance';

update Customer set State = 'Unknown' where State is null;

update Customer set SupportRepId = SupportRepId + 1 where FirstName like 'Lu%';
</screen>
</para>
<section>
<title>Ejercicios de UPDATE</title>
<itemizedlist>
<listitem>Actualiza el estado de los clientes a 'Madrid' en los registros donde la ciudad sea Madrid.</listitem>
<listitem>Actualiza el genero a 13 en las pistas cuyo compositor sea 'Skyes' y el nombre de las canciones termine con la palabra 'Love'</listitem>
</itemizedlist>
</section>
</section>

<section>
<title>Eliminando registros</title>
<para>La sentencia <quote>delete</quote> se usa para borrar (eliminar) registros de una tabla. Es posible definir los registros que van a ser eliminados construyendo cuidadosamente la clausula <quote>where</quote>
<screen>
delete from "nombre_tabla"

where "columna1" 
  OPERADOR "valor" 
[y|o "columna2" 
  OPERADOR "valor"];
</screen>
</para>
<para>
Ejemplos:
<screen>
delete from Invoice;
</screen>
<note>Si se omite la clausula <quote>where</quote> TODOS los registros en la tabla se eliminan.</note>
<screen>
delete from Playlist where Name = 'Movies';

delete from Playlist where Name like 'Classical%' or Name = 'Audiobooks';
</screen>
</para>

<section>
<title>Ejercicios DELETE</title>
<itemizedlist>
<listitem>Elimina de la tabla Track los registros cuyo Nombre empiece con '0' o  que el Autor sea 'Steve Harris'</listitem>
<listitem>Elimina de la tabla Customer los registros donde el Estado sea 'Unknown' y el país 'India'</listitem>
</itemizedlist>
</section>

</section>

<section>
<title>Eliminando tablas</title>
<para>La sentencia <quote>drop table</quote> se usa para eliminar tablas con todos sus registros.
Eliminando todos los registros con la sentencia <quote>delete</quote> deja la tabla vacía en cambio <quote>drop</quote> elimina la tabla completamente.
<screen>
drop table "nombre_tabla"
</screen></para>
<para>Ejemplo:
<screen>
drop table aficiones;

Drop table PlaylistTrack
</screen>
</para>
</section>

</chapter>
