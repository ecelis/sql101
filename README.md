SQL 101
Introductory (Spanish only) SQL crash course with examples and code for SQLite3.

At first this course was done for a company I was working for but it
never was taught actually, I'm planning to keep updating it since I
think its useful for some people I get know from time to time who are in
need to learn just enough SQL to query a data base without having to
turn into DBAs.

This works is released under the Creative Commons Attribution-ShareAlike
3.0 Unported License.

Example data license is in ChinookDatabase.LICENSE
