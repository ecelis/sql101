STYLESHEETS_DIR = ../docbook/stylesheet/vnova
TIDYCMD = tidy -i -m -xml -utf8
TMPDIR=/tmp
TARGET=SQLCourse
ISODIR=SQLCourse

all: clean html pdf

iso: clean html pdf
	rm -f $(TMPDIR)/$(TARGET).iso
	rm -f $(TMPDIR)/$(TARGET).zip
	rm -rf $(TMPDIR)/$(ISODIR)
	mkdir -p $(TMPDIR)/$(ISODIR)
	cp *.LICENSE $(TMPDIR)/$(ISODIR)
	cp *.COPY $(TMPDIR)/$(ISODIR)
	cp *.txt $(TMPDIR)/$(ISODIR)
	cp -r html $(TMPDIR)/$(ISODIR)
	cp $(TARGET).pdf $(TMPDIR)/$(ISODIR)
	cp -r Linux $(TMPDIR)/$(ISODIR)
	cp -r materiales $(TMPDIR)/$(ISODIR)
	cp -r Windows $(TMPDIR)/$(ISODIR)
	cp -r Mac $(TMPDIR)/$(ISODIR)
	cd $(TMPDIR) ; mkisofs -J -r -o $(TMPDIR)/$(TARGET).iso $(TMPDIR)/$(ISODIR) ; zip -r $(TMPDIR)/$(TARGET).zip $(ISODIR)

html:
	xsltproc --stringparam base.dir html/ -o $(TARGET).html $(STYLESHEETS_DIR)/html/chunk.xsl $(TARGET).xml
	
slides:
	xsltproc $(STYLESHEETS_DIR)/slides/html/default.xsl $(TARGET).xml
	
fo:
	xsltproc -o $(TARGET).fo $(STYLESHEETS_DIR)/fo/docbook.xsl $(TARGET).xml
	
pdf: fo
	fop -pdf $(TARGET).pdf -fo $(TARGET).fo
	
tidy: clean
	$(TIDYCMD) $(TARGET).xml

clean:
	rm -rf html *.fo *.pdf *.*~ .*.sw* *.iso
	find . -name '*.db' -exec rm {} \;
	find . -name '*.sqlite' -exec rm {} \;
